﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEditor;
using UnityEngine;

public class Target : MonoBehaviour
{
    private GameController gameController;
    private Rigidbody rigidbody;
    private bool targetIsMoving = false;
    public bool targetShotDown = false;
    
    private void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        gameController = FindObjectOfType<GameController>();
    }
    
    private void Update()
    {
        if (!rigidbody.IsSleeping())
        {
            targetIsMoving = true;
            targetShotDown = transform.position.y < -2;
            if (transform.position.y < -2)
            {
                gameController.CheckEndGame();
                if(this!=null)
                DestroyImmediate(gameObject);
                return;
            }
        }

        if (targetIsMoving 
        && rigidbody.IsSleeping())
        {
            targetIsMoving = false;
            gameController.CheckEndGame();
        }
    }
}