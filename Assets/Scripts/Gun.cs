﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

public class Gun : MonoBehaviour
{
    [SerializeField] private GameObject bullet;
    [SerializeField] private Vector3 startPosition;
    [SerializeField] private ProgressBar progressBar;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.gameObject.tag.Equals("Target"))
                {
                    Vector3 direction = hit.point - startPosition;
                    MakeShoot(direction, 60);
                }
            }
        }
    }

    /// <summary>
    ///  Произвести выстрел из пушки
    /// </summary>
    /// <param name="direction">направление выстрела</param>
    /// <param name="force">сила выстрела</param>
    public void MakeShoot(Vector3 direction, float force)
    {
        // если выстрелы кончились, не стрелять
        if (!progressBar.HasShots)
        {
            return;
        }

        progressBar.MakeShoot();
        GameObject _bullet = Instantiate(bullet, startPosition, Quaternion.identity);
        Rigidbody rg = _bullet.GetComponent<Rigidbody>();
        rg.AddForce(direction * force, ForceMode.Impulse);
    }
}
