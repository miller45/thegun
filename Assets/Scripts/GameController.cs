﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{

    [SerializeField] private PlatformController platform;
    [SerializeField] private ProgressBar progressBar;
    [SerializeField] private GameObject gameOverPopup;
    // указатель на текущий уровень
    public int currentLevel = 0;
    // макс. кол-во уровней
    public int maxLevelCount = 10;

    private void Start()
    {
        NextLevel();
    }

    /// <summary>
    /// Запустить следующий уровень при его прохождении,
    /// либо вывести экран окончания игры, если уровень последний
    /// </summary>
    private void NextLevel()
    {
        if (currentLevel != -1)
        {
            FacebookManager.Instance.LevelEnded(currentLevel+1);
        }

        currentLevel++;
        if (currentLevel < maxLevelCount)
        {
            gameOverPopup.SetActive(false);
            platform.LoadLevel();
            progressBar.LoadLevel();
        }
        else
        {
            GameOver();
        }
    }

    /// <summary>
    /// Проверка прохождения уровня,
    /// если пройден запуск следующего,
    /// если нет попап окончания игры
    /// </summary>
    public void CheckEndGame()
    {
        bool playerWin = platform.CheckEndLevel();
        if (playerWin)
        {
            NextLevel();
        }
        else
        {
            if (!progressBar.HasShots)
            {
                GameOver();
            }
        }
    }
    
    /// <summary>
    /// Окончание игры, показать попап завершения
    /// </summary>
    public void GameOver()
    {
        gameOverPopup.SetActive(true);
    }

    /// <summary>
    /// Стартовать игру с начала
    /// </summary>
    public void RestartGame()
    {
        currentLevel = -1;
        gameOverPopup.SetActive(false);
        NextLevel();
    }
}
