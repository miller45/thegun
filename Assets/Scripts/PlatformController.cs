﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Debug = UnityEngine.Debug;

public enum TargetType
{
    Empty,
    Box1,
    Box2
}

public class PlatformController : MonoBehaviour
{
    [SerializeField] private GameController gameController;
    [SerializeField] private GameObject content;
    [SerializeField] private GameObject box1;
    [SerializeField] private GameObject box2;
    private List<Target> items = new List<Target>();
    private TargetType[][,] levels;

    # region Levels

    private TargetType[,] level1 =
    {
        {TargetType.Empty, TargetType.Box1, TargetType.Box2, TargetType.Box1, TargetType.Empty},
        {TargetType.Empty, TargetType.Empty, TargetType.Empty, TargetType.Empty, TargetType.Empty},
        {TargetType.Empty, TargetType.Empty, TargetType.Empty, TargetType.Empty, TargetType.Empty}
    };

    private TargetType[,] level2 =
    {
        {TargetType.Box2, TargetType.Box1, TargetType.Box2, TargetType.Box1, TargetType.Box2},
        {TargetType.Empty, TargetType.Box2, TargetType.Box1, TargetType.Box2, TargetType.Empty},
        {TargetType.Empty, TargetType.Empty, TargetType.Empty, TargetType.Empty, TargetType.Empty}
    };

    private TargetType[,] level3 =
    {
        {TargetType.Box2, TargetType.Box1, TargetType.Box2, TargetType.Box1, TargetType.Box2},
        {TargetType.Box1, TargetType.Box2, TargetType.Box1, TargetType.Box2, TargetType.Box1},
        {TargetType.Empty, TargetType.Empty, TargetType.Empty, TargetType.Empty, TargetType.Empty}
    };

    private TargetType[,] level4 =
    {
        {TargetType.Box2, TargetType.Box1, TargetType.Box2, TargetType.Box1, TargetType.Box2},
        {TargetType.Box1, TargetType.Box2, TargetType.Box1, TargetType.Box2, TargetType.Box1},
        {TargetType.Empty, TargetType.Box1, TargetType.Box2, TargetType.Box1, TargetType.Empty}
    };

    private TargetType[,] level5 =
    {
        {TargetType.Box2, TargetType.Box1, TargetType.Box2, TargetType.Box1, TargetType.Box2},
        {TargetType.Box1, TargetType.Box2, TargetType.Box1, TargetType.Box2, TargetType.Box1},
        {TargetType.Box2, TargetType.Empty, TargetType.Box2, TargetType.Empty, TargetType.Box2}
    };

    private TargetType[,] level6 =
    {
        {TargetType.Box2, TargetType.Box1, TargetType.Box2, TargetType.Box1, TargetType.Box2},
        {TargetType.Box1, TargetType.Box2, TargetType.Box1, TargetType.Box2, TargetType.Box1},
        {TargetType.Empty, TargetType.Empty, TargetType.Box2, TargetType.Box1, TargetType.Box2},
        {TargetType.Empty, TargetType.Empty, TargetType.Empty, TargetType.Empty, TargetType.Box1}
    };

    private TargetType[,] level7 =
    {
        {TargetType.Box2, TargetType.Box1, TargetType.Box2, TargetType.Box1, TargetType.Box2},
        {TargetType.Box1, TargetType.Box2, TargetType.Box1, TargetType.Box2, TargetType.Box1},
        {TargetType.Box2, TargetType.Box1, TargetType.Empty, TargetType.Box1, TargetType.Box2},
        {TargetType.Box1, TargetType.Empty, TargetType.Empty, TargetType.Empty, TargetType.Box1}
    };

    private TargetType[,] level8 =
    {
        {TargetType.Box2, TargetType.Box1, TargetType.Box2, TargetType.Box1, TargetType.Box2},
        {TargetType.Box1, TargetType.Box2, TargetType.Box1, TargetType.Box2, TargetType.Box1},
        {TargetType.Box2, TargetType.Box1, TargetType.Box2, TargetType.Box1, TargetType.Box2},
        {TargetType.Box1, TargetType.Empty, TargetType.Box1, TargetType.Empty, TargetType.Box1}
    };

    private TargetType[,] level9 =
    {
        {TargetType.Box2, TargetType.Box1, TargetType.Box2, TargetType.Box1, TargetType.Box2},
        {TargetType.Box1, TargetType.Box2, TargetType.Empty, TargetType.Box2, TargetType.Box1},
        {TargetType.Box2, TargetType.Box1, TargetType.Empty, TargetType.Box1, TargetType.Box2},
        {TargetType.Box1, TargetType.Empty, TargetType.Empty, TargetType.Empty, TargetType.Box1}
    };

    private TargetType[,] level10 =
    {
        {TargetType.Box2, TargetType.Box1, TargetType.Box2, TargetType.Box1, TargetType.Box2},
        {TargetType.Box1, TargetType.Box2, TargetType.Box1, TargetType.Box2, TargetType.Box1},
        {TargetType.Box2, TargetType.Box1, TargetType.Box2, TargetType.Box1, TargetType.Box2},
        {TargetType.Box1, TargetType.Box2, TargetType.Box1, TargetType.Box2, TargetType.Box1}
    };

    #endregion

    private void Awake()
    {
        // заполняем массив уровней
        levels = new[] {level1, level2, level3, level4, level5, level6, level7, level8, level9, level10};
    }

    private void CreatePlatform(TargetType[,] levelInfo)
    {
        Target[] targets = content.GetComponentsInChildren<Target>();
        // очщаем объекты с прошлого уровня, если остались
//        for (int i = 0; i < items.Count; i++)
//        {
//            if (!items[i].Equals(null))
//            {
//                Destroy(items[i].gameObject);
////                DestroyImmediate(items[i]);
//            }
//        }
        for (int i = 0; i < targets.Length; i++)
        {
            DestroyImmediate(targets[i].gameObject);
        }

        items.Clear();

        // создаем объекты уровня
        int rows = levelInfo.GetUpperBound(0) + 1;
        int columns = levelInfo.Length / rows;
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                if (!levelInfo[i, j].Equals(TargetType.Empty))
                {
                    GameObject item = null;
                    switch (levelInfo[i, j])
                    {
                        case TargetType.Box1:
                            item = Instantiate(box1, new Vector3(-3 + 1.5f * j, 1.5f * i, 15), Quaternion.identity,
                                content.transform);
                            break;
                        case TargetType.Box2:
                            item = Instantiate(box2, new Vector3(-3 + 1.5f * j, 1.5f * i, 15), Quaternion.identity,
                                content.transform);
                            break;
                    }

                    if (!item.Equals(null))
                        items.Add(item.GetComponent<Target>());
                }
            }
        }
    }

    public bool CheckEndLevel()
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (!items[i].Equals(null)
                && !items[i].targetShotDown)
            {
                return false;
            }
        }

        return true;
    }

    public void LoadLevel()
    {
        CreatePlatform(levels[gameController.currentLevel]);
    }
}