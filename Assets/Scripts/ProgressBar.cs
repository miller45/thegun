﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    public Color done;
    public Color inProcess;
    public Color avaliable;
    [SerializeField] private GameController gameController;
    [SerializeField] private Text currentLevelText;
    [SerializeField] private Text nextLevel;
    [SerializeField] private GameObject shootItem;
    [SerializeField] private Transform content;

    public List<GameObject> shoots = new List<GameObject>();
    private int currentShoot;
    private int avaliableShots;

    public bool HasShots => avaliableShots > 0;

    /// <summary>
    /// Заполнение UI при загрузке следующего
    /// </summary>
    public void LoadLevel()
    {
        // Отображаем текущий и следующий уровни
        currentLevelText.text = (gameController.currentLevel+1).ToString();
        nextLevel.text = gameController.maxLevelCount != gameController.currentLevel + 1
            ? ( gameController.currentLevel + 2).ToString()
            : "X";
        
        // Очищаем объект от старых выстрелов
        for (int i = shoots.Count - 1 ; i > -1; i--)
        {
            DestroyImmediate(shoots[i]);
        }
        shoots.Clear();

        // Заполняем новыми встрелами
        avaliableShots = ( gameController.currentLevel + 1) * 3;
        for (int i = 0; i < avaliableShots; i++)
        {
            GameObject shoot = Instantiate(shootItem, content.transform);
            shoots.Add(shoot);
            shoot.GetComponent<Image>().color = i == 0
                ? inProcess
                : avaliable;
        }

        currentShoot = 0;
    }

    public void MakeShoot()
    {
        if (avaliableShots<=0)
        {
            return;
        }

        avaliableShots--;
        if(currentShoot<shoots.Count)
        shoots[currentShoot].GetComponent<Image>().color = done;
        currentShoot++;
        if(currentShoot<shoots.Count)
        shoots[currentShoot].GetComponent<Image>().color = inProcess;
    }
}
